#include <vector>
#include <string>

std::vector<std::string> strsplit(std::string str,char ch)
{
    int last_split = 0;
    std::vector<std::string> result;
    for (int i = 0; i < str.size(); i++)
    {
        if (str[i] == ch)
        {
            result.push_back(str.substr(last_split,i-last_split));
            last_split = i+1;
        }
        else if (i == str.size()-1)
        {
            result.push_back(str.substr(last_split,i-last_split+1));
        }
    }
    return result;
}

std::vector<std::string> strsplit(std::string str,std::string needle)
{
    int last_split = 0;
    std::vector<std::string> result;
    for (int i = 0; i < str.size()-needle.size(); i++)
    {
        if (str.substr(i,needle.size()).compare(needle) == 0 && i != str.size()-needle.size()-1)
        {
            result.push_back(str.substr(last_split,i-last_split));
            last_split = i+needle.size();
        }
        else if (i == str.size()-needle.size()-1 && str.substr(i,needle.size()).compare(needle) != 0)
        {
            result.push_back(str.substr(last_split,i-last_split+1));
        }
    }
    return result;
}
